package tiposdedados;

public class TiposDeDados {
    public static void main(String[] args){
        System.out.println("Tipos de Dados em Java\n");
        System.out.printf("byte: minimo[%s]\tmaximo[%s]\n",
                Byte.MIN_VALUE, Byte.MAX_VALUE);
        System.out.printf("short: minimo[%s]\tmaximo[%s]\n",
                Short.MIN_VALUE, Short.MAX_VALUE);
        System.out.printf("int: minimo[%s]\tmaximo[%s]\n",
                Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.printf("long: minimo[%s]\tmaximo[%s]\n",
                Long.MIN_VALUE, Long.MAX_VALUE);
        System.out.printf("float: minimo[%s]\tmaximo[%s]\n",
                Float.MIN_VALUE, Float.MAX_VALUE);
        System.out.printf("double: minimo[%s]\tmaximo[%s]\n",
                Double.MIN_VALUE, Double.MAX_VALUE);
        System.out.printf("boolean: minimo[%s]\tmaximo[%s]\n",
                Boolean.FALSE, Boolean.TRUE);
        System.out.printf("char: minimo[%s]\tmaximo[%s]\n",
                Character.MIN_VALUE, Character.MAX_VALUE);

        byte mByte = 127;
        short mShort = 32767;

    }
}
