package operadores;

public class Operadores {

    public static void main(String[] args){
        System.out.printf("Operadores Aritmeticos\n");
        System.out.printf("10 + 5 = %d\n", (10 + 5));
        System.out.printf("10 - 5 = %d\n", (10 - 5));
        System.out.printf("10 * 5 = %d\n", (10 * 5));
        System.out.printf("10 / 5 = %d\n", (10 / 5));
        System.out.printf("10 %% 4 = %d\n", (10 % 4));

        System.out.printf("\n\nOperadores Relacionais\n");
        System.out.printf("10 > 5 = %B\n", (10 > 5));
        System.out.printf("10 < 5 = %B\n", (10 < 5));
        System.out.printf("10 >= 5 = %B\n", (10 >= 5));
        System.out.printf("10 <= 5 = %B\n", (10 <= 5));
        System.out.printf("10 != 5 = %B\n", (10 != 5));
        System.out.printf("10 == 5 = %B\n", (10 == 5));

        System.out.printf("\n\nOperadores Logicos\n");
        System.out.printf("\nOperadores E\n");
        System.out.printf("V E V = %B\n", true && true);
        System.out.printf("V E F = %B\n", true && false);
        System.out.printf("F E V = %B\n", false && true);
        System.out.printf("F E F = %B\n", false && false);

        System.out.printf("\nOperadores OU\n");
        System.out.printf("V OU V = %B\n", true || true);
        System.out.printf("V OU F = %B\n", true || false);
        System.out.printf("F OU V = %B\n", false || true);
        System.out.printf("F OU F = %B\n", false || false);

        System.out.printf("\nOperadores XOU\n");
        System.out.printf("V XOU V = %B\n", true ^ true);
        System.out.printf("V XOU F = %B\n", true ^ false);
        System.out.printf("F XOU V = %B\n", false ^ true);
        System.out.printf("F XOU F = %B\n", false ^ false);

        System.out.printf("\nOperadores NAO\n");
        System.out.printf("NAO V = %B\n", !true);
        System.out.printf("NAO F = %B\n", !false);

        System.out.printf("\nOperadores de Atribuição\n");
        int a = 10;
        int b = 5;
        System.out.printf("a = 10 e b = 5\n");
        System.out.printf("%s = %s: %s\n", a, b, (a = b));
        System.out.printf("%s += %s: %s\n", a, b, (a += b));
        System.out.printf("%s -= %s: %s\n", a, b, (a -= b));
        System.out.printf("%s *= %s: %s\n", a, b, (a *= b));
        System.out.printf("%s /= %s: %s\n", a, b, (a /= b));
        System.out.printf("%s %%= %s: %s\n", a, b, (a %= b));

        System.out.printf("\nOperadores de Incremento\n");
        int contador = 1;
        System.out.println("contador: "+ --contador);
        System.out.println("contador agora: "+ contador);
    }
}
